@extends('app')
@section('content')
    <div >Hi, {{ auth()->user()->name }} </div>

    <div></div>
    <div><a href="{{url('/create')}}">Make new todo...</a></div>
    <div></div>

    @foreach($todos as $todo)
        <div>        
            {{$todo['desc']}} <a href="{{url('/edit')}}{{ '/'.$todo['id']}}">Edit</a> <a href="{{url('/delete')}}{{ '/'.$todo['id']}}">Delete</a>
        </div>
    @endforeach
@endsection