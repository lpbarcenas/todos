<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">

<body>
    <div class="content">

        @yield('content')

        @if (auth()->user())
            <form name="logoutForm" method="post" action="{{ route('logout') }}">

                {{ csrf_field() }}

                <button type="submit">
                    LOG OUT
                </button>
            </form>
        @endif
    </div>
</body>
</html>
