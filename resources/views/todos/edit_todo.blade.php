@extends('app')
@section('content')
    <div >Edit todo</div>
    <form name="editTodoForm" method="post" action="{{ route('update-todo') }}">

        {{ csrf_field() }}

        <div>
            <label for="desc">Content</label>
            <input type="text" name="desc" id="desc" placeholder="Description" value="{{$desc}}"  size="60" maxlength="150" required />
        </div>

        <input type="hidden" name="id" id="id" value="{{$id}}" />

        <button type="submit">
            Submit
        </button>
    </form>
@endsection