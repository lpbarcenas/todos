@extends('app')
@section('content')
    <div >Create todo</div>
    <form name="newTodoForm" method="post" action="{{ route('create-todo') }}">

        {{ csrf_field() }}

        <div>
            <label for="desc">Content</label>
            <input type="text" name="desc" id="desc" placeholder="Description" size="60"  maxlength="150" required />
            
        </div>

        <button type="submit">
            Submit
        </button>
    </form>
@endsection