@extends('app')
@section('content')
<div>Log in to your account</div>

    <form name="loginForm" method="post" action="{{ route('login') }}">

        {{ csrf_field() }}

        <div>
            <label for="loginFormInputUsername">Email</label>
            <input type="email" name="email" id="loginFormInputUsername" placeholder="Email" required />

        </div>

        <div>
            <label for="loginFormInputPassword">Password</label>
            <input type="password" name="password" id="loginFormInputPassword" placeholder="Password" required />

        </div>

        <button type="submit">
            LOG IN
        </button>
    </form>

@endsection