<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'Auth\LoginController@showLoginForm');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/create', 'HomeController@viewCreateForm')->name('view-create-form');
Route::post('/create', 'HomeController@postCreate')->name('create-todo');

Route::get('/edit/{id}', 'HomeController@viewUpdateForm')->name('view-update-form');
Route::post('/update', 'HomeController@postUpdate')->name('update-todo');

Route::get('/delete/{id}', 'HomeController@deleteTodo')->name('delete-todo');
