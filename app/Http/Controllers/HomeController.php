<?php

namespace App\Http\Controllers;


use App\User;
use App\Models\Todo;


class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $user = auth()->user();
        $myTodos = $user->todos()->get()->toArray();

        return view('home')->with(["todos"=>$myTodos]);
    }

    public function viewCreateForm() {
        return view('todos.create_todo');
    }

    public function viewUpdateForm($id) {

        $todo = Todo::find($id);
        
        return view('todos.edit_todo')->with(['desc'=>$todo->desc, 'id'=>$todo->id]);
    }

    public function postCreate() {
        $desc = \request()->input('desc');

        $todo = new Todo();
        $todo->user_id = auth()->user()->id;
        $todo->desc = $desc;
        $todo->save();

        return redirect()->route('home');

    }

    public function postUpdate() {
        $desc = \request()->input('desc');
        $id = \request()->input('id');
        
        $todo = Todo::find($id);

        if ($todo->user_id==auth()->user()->id) {
            $todo->desc = $desc;
            $todo->save();
        }

        return redirect()->route('home');

    }

    public function deleteTodo($id) {
        
        $todo = Todo::find($id);

        if ($todo->user_id==auth()->user()->id) {
            $todo->delete();
        }

        return redirect()->route('home');

    }

}