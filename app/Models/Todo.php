<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    protected $fillable = [
        'desc', 'user_id'
    ];

    protected $table = "todo";

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
   
}
